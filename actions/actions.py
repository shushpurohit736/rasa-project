# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

# from typing import Any, Text, Dict, List
#
# from rasa_sdk import Action, Tracker
# from rasa_sdk.executor import CollectingDispatcher
#
#
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import openai

start_chat_log = '''Human: Hello, how are you?
Bot: I am doing great. How can I help you today?
'''

key = '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'

con = 'This is a test message for consultation.'

fin = 'This is a test message for financials.'

mar = 'This is a test message for marketing.'

def answer(question, key, chat_log=None):
    # initiating the chat log
    if chat_log is None:
        chat_log = start_chat_log
        # getting the apikey
    openai.api_key = key
    # getting the quest from the user
    prompt = f'{chat_log}Human: {question}\nBot:'
    # creating an object tp get the reply from open ai
    response = openai.Completion.create(engine="text-davinci-002", prompt=prompt, temperature=0.3,
                                        max_tokens=64, top_p=1, frequency_penalty=0.5,
                                        presence_penalty=0)
    # getting the choices from content recieved
    content = response.choices
    return content


def append_chat_log(question, answer, chat_log=None):
    if chat_log is None:
        chat_log = start_chat_log
    # appending the chatlog with quest and answer
    return f'{chat_log}Human: {question}\nAI: {answer}\n'


def get_reply(key, quest):
    # appending the chat log to previous chat
    res1 = answer(quest, key)[0].text.strip()
    chat_log = append_chat_log(quest, res1)
    return res1


key = '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'

ML = 'Machine learning is the study of computer algorithms that can improve automatically through experience and by the use of data. It is seen as a part of artificial intelligence.'

DL = 'Deep learning is a machine learning technique that teaches computers to do what comes naturally to humans: learn by example. Deep learning is a key technology behind driverless cars, enabling them to recognize a stop sign, or to distinguish a pedestrian from a lamppost.'

DB = 'A dashboard is a type of graphical user interface which often provides at-a-glance views of key performance indicators (KPIs) relevant to a particular objective or business process. In other usage, "dashboard" is another name for "progress report" or "report" and considered a form of data visualization. In providing this overview, business owners can save time and improve their decision making by utilizing dashboards.'

DE = 'Data engineering is the complex task of making raw data usable to data scientists and groups within an organization. Data engineering encompasses numerous specialties of data science.'

AI = 'Artificial intelligence (AI) is intelligence demonstrated by machines, as opposed to the natural intelligence displayed by animals including humans. AI research has been defined as the field of study of intelligent agents, which refers to any system that perceives its environment and takes actions that maximize its chance of achieving its goals.'



class ActionReplyField(Action):

    def name(self) -> Text:
        return "action_reply_field"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # storing the list of interest extracted from user input
        chosen_field =  (tracker.get_slot("field"))

        # for consultation field
        if chosen_field == 'con':
            msg = con
            dispatcher.utter_message(text=msg)

        # doe financials field
        elif chosen_field == 'fin':
            msg = fin
            dispatcher.utter_message(text=msg)

        #  for marketing field
        elif chosen_field == 'mar':
            msg = mar
            dispatcher.utter_message(text=msg)

        return []

class ActionSendLink(Action):
    def name(self) -> Text:
        return "action_dynamic_link"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(template='utter_link')
        return []

class ActionAnswerOpen(Action):

    def name(self) -> Text:
        return "action_answer_open"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # storing the list of interest extracted from user input
        user_quest = (tracker.latest_message['text'])

        # getting reply from gpt
        reply = get_reply(key, user_quest)

        # uttering reply from gpt
        dispatcher.utter_message(text=reply)

        return []


from typing import Text, List, Any, Dict
from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.events import EventType
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
import re

regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'


def check(email):
    if (re.fullmatch(regex, email)):
        x = "yes"
    else:
        x = "no"
    return x

class ValidateInfoForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_info_form"

    def validate_name(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `name` value."""

        if type(slot_value) != str:
            dispatcher.utter_message(text=f"Please provide your proper name.")
            return {"name": None}
        dispatcher.utter_message(text=f"OKAY great hi {slot_value}.")
        return {"name": slot_value}

    def validate_email(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `email` value."""

        if check(slot_value) == 'no':
            dispatcher.utter_message(text=f"Please enter the proper email")
            return {"email": None}
        dispatcher.utter_message(text=f"OK! Your email id is {slot_value}")
        return {"email": slot_value}

class ValidateloopForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_loop_form"

    def validate_interesting(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `interesting` value."""

        if type(slot_value) != str:
            dispatcher.utter_message(text=f"Please enter the proper interest")
            return {"interesting": None}
        # getting reply from gpt
        reply = get_reply(key,slot_value)
        # uttering reply from gpt
        dispatcher.utter_message(text=f"{reply}")
        return {"select": None,"interesting":slot_value}



    def validate_select(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `select` value."""

        if type(slot_value) != str:
            dispatcher.utter_message(text=f"opps! no able to get you, pardon please")
            return {"select": None}
        if slot_value == 'yes':
            dispatcher.utter_message(text=f"love to see that you wanna know more about this things ")
            return {"interesting": None}
        if slot_value == 'no':
            dispatcher.utter_message(text=f"we would love to introduce you to our bd team")
            return {"select": slot_value}
